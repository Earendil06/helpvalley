import 'dart:typed_data';

import 'package:helpvalley/models.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;

const baseColor = PdfColors.teal;
const accentColor = PdfColors.blueGrey900;
const _darkColor = PdfColors.blueGrey800;
const _lightColor = PdfColors.white;

_baseTextColor() => baseColor.luminance < 0.5 ? _lightColor : _darkColor;


Future<Uint8List> generatePdf(
    Demand demand, List<UserProfile> people, PdfPageFormat pageFormat) async {
  final doc = pw.Document();
  doc.addPage(
    pw.MultiPage(
      header: _buildHeader,
      footer: _buildFooter,
      build: (context) => [
        _contentHeader(demand, context),
        _contentTable(people, context),
        pw.SizedBox(height: 20),
        _contentFooter(context),
        pw.SizedBox(height: 20),
        _termsAndConditions(context),
      ],
    ),
  );

  return doc.save();
}

pw.Widget _buildHeader(pw.Context context) {
  return pw.Column(
    children: [
      pw.Row(
        crossAxisAlignment: pw.CrossAxisAlignment.start,
        children: [
          pw.Expanded(
            child: pw.Column(
              children: [
                pw.Container(
                  height: 50,
                  padding: const pw.EdgeInsets.only(left: 20),
                  alignment: pw.Alignment.center,
                  child: pw.Text(
                    'AIDE1VALLEE',
                    style: pw.TextStyle(
                      color: baseColor,
                      fontWeight: pw.FontWeight.bold,
                      fontSize: 40,
                    ),
                  ),
                ),
              ],
            ),
          ),
          pw.Expanded(
            child: pw.Column(
              mainAxisSize: pw.MainAxisSize.min,
              children: [],
            ),
          ),
        ],
      ),
      if (context.pageNumber > 1) pw.SizedBox(height: 20)
    ],
  );
}

pw.Widget _buildFooter(pw.Context context) {
  return pw.Row(
    mainAxisAlignment: pw.MainAxisAlignment.spaceBetween,
    crossAxisAlignment: pw.CrossAxisAlignment.end,
    children: [
      pw.Container(
        height: 20,
        width: 100,
      ),
      pw.Text(
        'Page ${context.pageNumber}/${context.pagesCount}',
        style: const pw.TextStyle(
          fontSize: 12,
          color: PdfColors.grey,
        ),
      ),
    ],
  );
}

pw.Widget _contentHeader(Demand demand, pw.Context context) {
  return pw.Row(
    crossAxisAlignment: pw.CrossAxisAlignment.start,
    children: [
      pw.Expanded(
        child: pw.Container(),
      ),
      pw.Expanded(
        child: pw.Row(
          children: [
            pw.Container(
              margin: const pw.EdgeInsets.only(left: 10, right: 10, top: 20),
              height: 70,
              child: pw.Text(
                'Mission:',
                style: pw.TextStyle(
                  color: _darkColor,
                  fontWeight: pw.FontWeight.bold,
                  fontSize: 12,
                ),
              ),
            ),
            pw.Expanded(
              child: pw.Container(
                height: 70,
                margin: const pw.EdgeInsets.only(top: 20),
                child: pw.RichText(
                    text: pw.TextSpan(
                        text: '${demand.summary}\n',
                        style: pw.TextStyle(
                          color: _darkColor,
                          fontWeight: pw.FontWeight.bold,
                          fontSize: 12,
                        ),
                        children: [
                      pw.TextSpan(
                        text: '\n',
                        style: pw.TextStyle(
                          fontSize: 5,
                        ),
                      ),
                      pw.TextSpan(
                        text: demand.date,
                        style: pw.TextStyle(
                          fontWeight: pw.FontWeight.normal,
                          fontSize: 10,
                        ),
                      ),
                    ])),
              ),
            ),
          ],
        ),
      ),
    ],
  );
}

pw.Widget _contentFooter(pw.Context context) {
  return pw.Row(
    crossAxisAlignment: pw.CrossAxisAlignment.start,
    children: [
      pw.Expanded(
        flex: 2,
        child: pw.Column(
          crossAxisAlignment: pw.CrossAxisAlignment.start,
          children: [
            pw.Text(
              'Merci pour votre participation',
              style: pw.TextStyle(
                color: _darkColor,
                fontWeight: pw.FontWeight.bold,
              ),
            ),
          ],
        ),
      ),
      pw.Expanded(
        flex: 1,
        child: pw.DefaultTextStyle(
          style: const pw.TextStyle(
            fontSize: 10,
            color: _darkColor,
          ),
          child: pw.Column(
            crossAxisAlignment: pw.CrossAxisAlignment.start,
            children: [],
          ),
        ),
      ),
    ],
  );
}

pw.Widget _termsAndConditions(pw.Context context) {
  return pw.Row(
    children: [],
  );
}

pw.Widget _contentTable(List<UserProfile> people, pw.Context context) {
  const tableHeaders = [
    'Nom',
    'Prénom',
    'Téléphone',
  ];

  people.sort((a, b) => a.lastName.compareTo(b.lastName));

  return pw.Table.fromTextArray(
    border: null,
    cellAlignment: pw.Alignment.centerLeft,
    headerDecoration: pw.BoxDecoration(
      color: baseColor,
    ),
    headerHeight: 25,
    cellHeight: 40,
    cellAlignments: {
      0: pw.Alignment.centerLeft,
      1: pw.Alignment.centerLeft,
      2: pw.Alignment.centerRight,
    },
    headerStyle: pw.TextStyle(
      color: _baseTextColor(),
      fontSize: 10,
      fontWeight: pw.FontWeight.bold,
    ),
    cellStyle: const pw.TextStyle(
      color: _darkColor,
      fontSize: 10,
    ),
    rowDecoration: pw.BoxDecoration(
      border: pw.BoxBorder(
        bottom: true,
        color: accentColor,
        width: .5,
      ),
    ),
    headers: List<String>.generate(
      tableHeaders.length,
      (col) => tableHeaders[col],
    ),
    data: List<List<String>>.generate(
      people.length,
      (row) => List<String>.generate(
        tableHeaders.length,
        (col) => getIndex(people[row], col),
      ),
    ),
  );
}

getIndex(UserProfile profile, int index) {
  switch (index) {
    case 0:
      return profile.lastName;
    case 1:
      return profile.firstName;
    case 2:
      return profile.prettyTel;
  }
}
