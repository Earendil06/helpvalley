import 'package:flutter/material.dart';

showAlertDialog(BuildContext context,
    {Function onAccept,
      Widget title,
      Widget body,
      String cancelText,
      String acceptText}) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        title: title,
        content: body,
        actions: [
          FlatButton(
            child: Text(cancelText),
            onPressed: () {
              Navigator.of(context).pop();
            },
          ),
          FlatButton(
            child: Text(acceptText),
            onPressed: () {
              Navigator.of(context).pop();
              onAccept();
            },
          ),
        ],
      );
    },
  );
}
