import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:helpvalley/RemoteConfigEngine.dart';
import 'package:helpvalley/components/DatePicker.dart';

class FirebaseUtils {
  static T safeFieldGetter<T>(DocumentSnapshot doc, String key, {T orElse}) {
    if (doc == null || doc.data() == null) return orElse;
    return doc.data().containsKey(key) ? doc[key] as T : orElse;
  }

  static List<T> safeListGetter<T>(DocumentSnapshot doc, String key,
      {List<T> orElse}) {
    if (doc == null || doc.data() == null) return orElse;
    return doc.data().containsKey(key)
        ? (doc[key] as List).map((e) => e as T).toList()
        : orElse;
  }
}

class Donation {
  String _name;
  String _link;

  Donation(DocumentSnapshot document) {
    this._name = FirebaseUtils.safeFieldGetter(document, "name",
        orElse: "no "
            "title");
    this._link = FirebaseUtils.safeFieldGetter(document, "link",
        orElse: "no "
            "link");
  }

  Map<String, dynamic> toFirestoreModel() {
    return {"name": _name, "link": _link};
  }

  String get name => _name.trim();

  String get link => _link.trim();
}

class UserProfile {
  String _lastName;
  String _firstName;
  String _email;
  String _tel;
  bool displayTel;
  bool verificationEnabled;
  String role;

  List<String> notificationsEnabled;

  UserProfile(DocumentSnapshot document) {
    this._lastName =
        FirebaseUtils.safeFieldGetter(document, "lastName", orElse: "");
    this._firstName =
        FirebaseUtils.safeFieldGetter(document, "firstName", orElse: "");
    this._email = FirebaseUtils.safeFieldGetter(document, "email", orElse: "");
    this._tel = FirebaseUtils.safeFieldGetter(document, "tel", orElse: "");
    this.displayTel =
        FirebaseUtils.safeFieldGetter(document, "displayTel", orElse: false);
    this.role = FirebaseUtils.safeFieldGetter(document, "role", orElse: "");
    this.verificationEnabled = FirebaseUtils.safeFieldGetter(
        document, "verificationEnabled",
        orElse: false);

    this.notificationsEnabled = FirebaseUtils.safeListGetter(
        document, "notificationsEnabled",
        orElse: []);
  }

  Map<String, dynamic> toFirestoreModel() {
    return {
      "lastName": _lastName,
      "firstName": _firstName,
      "tel": _tel,
      "email": _email,
      "displayTel": displayTel,
      "verificationEnabled": verificationEnabled,
      "notificationsEnabled": notificationsEnabled,
      "role": role
    };
  }

  String get firstName =>
      "${_firstName.trim()[0].toUpperCase()}${_firstName.trim().substring(1).toLowerCase()}";

  set firstName(String firstName) {
    this.firstName = firstName;
  }

  set lastName(String lastName) {
    this.lastName = lastName;
  }

  bool get isOfficial => role == "official";

  String get lastName => _lastName.trim().toUpperCase();

  String get tel => _tel;

  String get prettyTel =>
      "+33 " +
      _tel
          .trim()
          .replaceAllMapped(RegExp(r".{2}"), (match) => "${match.group(0)} ")
          .substring(1);

  String get email => _email;
}

class Demand {
  String id;

  bool official = false;
  bool officialAide1Vallee = false;
  List<String> people = [];
  String userID = "";
  String material = "";
  String summary = "";
  String title = RemoteConfigEngine.categories().first;
  String information = "";
  String place = RemoteConfigEngine.tabs().first;
  String date = MyTextFieldDatePicker.toFormattedString(DateTime.now());
  int duration = 0;
  int numberRequired = 0;
  bool canceled = false;
  String village = "";
  bool hardAccess = false;
  bool archived = false;

  Demand();

  Demand.fromFirestore(DocumentSnapshot e) {
    id = e.id;
    canceled = FirebaseUtils.safeFieldGetter(e, "canceled", orElse: false);
    date = FirebaseUtils.safeFieldGetter(e, "date", orElse: "01-01-2000");
    duration = FirebaseUtils.safeFieldGetter(e, "duration", orElse: 0);
    hardAccess = FirebaseUtils.safeFieldGetter(e, "hardAccess", orElse: false);
    information = FirebaseUtils.safeFieldGetter(e, "information", orElse: "");
    material = FirebaseUtils.safeFieldGetter(e, "material", orElse: "");
    numberRequired =
        FirebaseUtils.safeFieldGetter(e, "numberRequired", orElse: 0);
    official = FirebaseUtils.safeFieldGetter(e, "official", orElse: false);
    officialAide1Vallee = FirebaseUtils.safeFieldGetter(e, "officialAide1Vallee", orElse: false);
    people = FirebaseUtils.safeListGetter(e, "people", orElse: []);
    place = FirebaseUtils.safeFieldGetter(e, "place", orElse: "");
    summary = FirebaseUtils.safeFieldGetter(e, "summary", orElse: "");
    title = FirebaseUtils.safeFieldGetter(e, "title", orElse: "");
    userID = FirebaseUtils.safeFieldGetter(e, "userID", orElse: "");
    village = FirebaseUtils.safeFieldGetter(e, "village", orElse: "");
    archived = FirebaseUtils.safeFieldGetter(e, "archived", orElse: false);
  }

  Map<String, dynamic> toFirestoreModel() {
    return {
      "canceled": canceled,
      "date": date,
      "duration": duration,
      "hardAccess": hardAccess,
      "information": information,
      "material": material,
      "numberRequired": numberRequired,
      "official": official,
      "officialAide1Vallee": officialAide1Vallee,
      "people": people,
      "place": place,
      "summary": summary,
      "title": title,
      "userID": userID,
      "village": village,
      "archived": archived
    };
  }

  bool isPastOrArchived() {
    if(canceled && !archived) {
      return false;
    }
    var d = MyTextFieldDatePicker.parseDate(date);
    return d.isBefore(DateTime.now().subtract(Duration(days: 1))) || archived;
  }

  bool isFutureOrToday() {
    var d = MyTextFieldDatePicker.parseDate(date);
    return d.isAfter(DateTime.now().subtract(Duration(days: 1)));
  }

  int computeJ() {
    var d1 = MyTextFieldDatePicker.parseDate(
        MyTextFieldDatePicker.toFormattedString(DateTime.now()));
    var d2 = MyTextFieldDatePicker.parseDate(date);
    return DateTimeRange(start: d1, end: d2).duration.inDays;
  }
}

class NotificationType {
  String name;
  String id;

  NotificationType.fromFirestore(QueryDocumentSnapshot e) {
    id = e.id;
    name = FirebaseUtils.safeFieldGetter(e, "name", orElse: "toto");
  }
}
