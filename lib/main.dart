import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:helpvalley/DynamicLinkService.dart';
import 'package:helpvalley/MessagingEngine.dart';
import 'package:helpvalley/RemoteConfigEngine.dart';
import 'package:helpvalley/RoutingEngine.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);

  await Firebase.initializeApp();

  FirebaseAuth.instance.currentUser?.reload();

  await MessagingEngine.initialize();
  await RemoteConfigEngine.initialize(const Duration(seconds: 5));
  await initDynamicLinks();

  runApp(GetMaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        textTheme: GoogleFonts.ubuntuCondensedTextTheme(),
      ),
      localizationsDelegates: [GlobalMaterialLocalizations.delegate],
      supportedLocales: [const Locale('en'), const Locale('fr')],
      home: RoutingEngine()));
}

Future<void> initDynamicLinks() async {
  FirebaseDynamicLinks.instance.onLink(
      onSuccess: (PendingDynamicLinkData dynamicLink) async {
    RoutingEngine()
        .routeUserToPage(FirebaseAuth.instance.currentUser, data: dynamicLink);
  }, onError: (OnLinkErrorException e) async {
    print('onLinkError');
    print(e.message);
  });
}
