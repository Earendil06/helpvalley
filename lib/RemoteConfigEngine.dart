import 'dart:convert';

import 'package:firebase_remote_config/firebase_remote_config.dart';

class RemoteConfigEngine {

  static RemoteConfig instance;

  static Future<RemoteConfig> initialize(Duration expiration) async {
    final RemoteConfig remoteConfig = await RemoteConfig.instance;
    final defaults = <String, dynamic>{
      'tabs': [],
      'categories': [],
      'email': ""
    };
    await remoteConfig.setDefaults(defaults);

    await remoteConfig.fetch(expiration: expiration);
    await remoteConfig.activateFetched();
    instance = remoteConfig;
    return remoteConfig;
  }

  static List<String> tabs() => (jsonDecode(RemoteConfigEngine.instance.getString
    ("tabs")) as List).map((e) => e.toString()).toList();

  static List<String> categories() => (jsonDecode(RemoteConfigEngine.instance
      .getString
    ("categories")) as List).map((e) => e.toString()).toList();
}
