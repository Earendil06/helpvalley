import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:helpvalley/components/DatePicker.dart';
import 'package:helpvalley/components/ListOfDemands.dart';
import 'package:helpvalley/components/RenderDemandTile.dart';
import 'package:intl/intl.dart';

class MyParticipations extends StatelessWidget {
  String userID;

  MyParticipations(this.userID);

  @override
  Widget build(BuildContext context) {
    Query myDemands =
        FirebaseFirestore.instance.collection('demands')
    .where("people", arrayContains: userID);

    return Scaffold(
        appBar: AppBar(title: Text("Mes participations")),
        body: ListOfDemands(
          renderNone: () => Text("Vous n'avez pas encore participé à une "
              "demande."),
          renderTile: (d) => RenderDemandTile(d),
          processList: (ll) {
            var l = ll
                .where((element) => element.people.contains(this.userID))
                .toList();
            l.sort((a, b) => MyTextFieldDatePicker.parseDate(a.date)
                .compareTo(MyTextFieldDatePicker.parseDate(b.date)));
            return l;
          },
          query: myDemands,
        ));
  }
}
