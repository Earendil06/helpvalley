import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:helpvalley/components/InfoRenderers.dart';
import 'package:helpvalley/models.dart';
import 'package:helpvalley/screens/profile/EditProfile.dart';
import 'package:helpvalley/screens/profile/MyDemands.dart';
import 'package:helpvalley/screens/profile/MyParticipations.dart';

class ViewProfile extends StatelessWidget {
  @override
  Widget build(context) {
    DocumentReference user = FirebaseFirestore.instance
        .collection('users')
        .doc(FirebaseAuth.instance.currentUser.uid);

    return Scaffold(
        appBar: AppBar(title: Text("Profil utilisateur")),
        body: StreamBuilder(
          stream: user.snapshots(),
          builder:
              (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
            if (!snapshot.hasData) {
              return Text("Loading");
            }
            var user = UserProfile(snapshot.data);
            return SingleChildScrollView(
                child: Column(children: [
              InfoRendererText(
                title: "Prénom",
                text: user.firstName,
              ),
              InfoRendererText(
                title: "Nom",
                text: user.lastName,
              ),
              InfoRendererText(
                title: "Email",
                text: user.email,
              ),
              InfoRendererText(
                title: "Téléphone",
                text: user.prettyTel,
              ),
              InfoRendererWidget(
                title: "Type de compte",
                widget: Chip(
                  label: Text(user.isOfficial
                      ? "OFFICIEL"
                      : "NORMAL"),
                  backgroundColor:
                      user.isOfficial
                          ? Colors.green
                          : Colors.grey,
                ),
              ),
              InfoRendererWidget(
                title: "Notifications (cliquer pour activer/désactiver)",
                widget: DisplayNotificationsSettings(user),
              ),
              InfoRendererWidget(
                  title: "Actions",
                  widget: Column(
                    children: [
                      TextButton(
                          onPressed: () => Get.to(EditProfile(user)),
                          child: new Text("Modifier le profil")),
                      TextButton(
                          onPressed: () => FirebaseAuth.instance.signOut(),
                          child: new Text("Se déconnecter")),
                      TextButton(
                          onPressed: () => Get.to(
                              MyDemands(FirebaseAuth.instance.currentUser.uid)),
                          child: Text("Mes demandes")),
                      TextButton(
                          onPressed: () => Get.to(MyParticipations(
                              FirebaseAuth.instance.currentUser.uid)),
                          child: Text("Mes participations")),
                      user.verificationEnabled
                          ? TextButton(
                              child: Text("Rendre officiel"),
                              onPressed: () => FirebaseAuth.instance.currentUser
                                  .sendEmailVerification())
                          : Text("")
                    ],
                  )),
            ]));
          },
        ));
    // );
    // );
  }
}

@immutable
class DisplayNotificationsSettings extends StatefulWidget {
  final UserProfile user;

  DisplayNotificationsSettings(this.user);

  @override
  _DisplayNotificationsSettingsState createState() =>
      _DisplayNotificationsSettingsState();
}

class _DisplayNotificationsSettingsState
    extends State<DisplayNotificationsSettings> {
  Stream<QuerySnapshot> notificationsTypes;

  @override
  void initState() {
    super.initState();
    notificationsTypes =
        FirebaseFirestore.instance.collection("notificationsTypes").snapshots();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: notificationsTypes,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text('Something went wrong');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Text("Loading");
        }

        var l = snapshot.data.docs
            .map((e) => NotificationType.fromFirestore(e))
            .toList();

        if (l.isNotEmpty) {
          return Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: l.map((notificationType) {
                      bool activated = widget.user.notificationsEnabled
                          .contains(notificationType.id);
                      return GestureDetector(
                        onTap: () {
                          if (activated) {
                            deactivateNotification(notificationType.id);
                          } else {
                            activateNotification(notificationType.id);
                          }
                        },
                        child: Container(
                            padding: const EdgeInsets.only(top: 7),
                            child: Row(
                              children: [
                                Icon(
                                  activated ? Icons.check : Icons.cancel,
                                  color: activated ? Colors.green : Colors.red,
                                ),
                                VerticalDivider(
                                  color: Colors.transparent,
                                ),
                                Expanded(child: Text(notificationType.name,
                                    style: TextStyle(
                                        color: activated
                                            ? Colors.green
                                            : Colors.red)))
                              ],
                            )),
                      );
                    }).toList(),
                  );
        } else {
          return Text("pas de notifs dispo");
        }
      },
    );
  }

  void deactivateNotification(String id) {
    FirebaseFirestore.instance
        .collection("users")
        .doc(FirebaseAuth.instance.currentUser.uid)
        .update({
      "notificationsEnabled": FieldValue.arrayRemove([id])
    });
  }

  void activateNotification(String id) {
    FirebaseFirestore.instance
        .collection("users")
        .doc(FirebaseAuth.instance.currentUser.uid)
        .update({
      "notificationsEnabled": FieldValue.arrayUnion([id])
    });
  }
}
