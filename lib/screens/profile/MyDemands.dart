import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:helpvalley/components/DatePicker.dart';
import 'package:helpvalley/components/ListOfDemands.dart';
import 'package:helpvalley/components/RenderDemandTile.dart';
import 'package:intl/intl.dart';

class MyDemands extends StatelessWidget {
  String userID;

  MyDemands(this.userID);

  @override
  Widget build(BuildContext context) {
    Query users = FirebaseFirestore.instance
        .collection('demands')
        .where("userID", isEqualTo: userID);

    return Scaffold(
        appBar: AppBar(title: Text("Mes demandes")),
        body: ListOfDemands(
          query: users,
          processList: (l) {
            l.sort((a, b) => MyTextFieldDatePicker.parseDate(a.date)
                .compareTo(MyTextFieldDatePicker.parseDate(b.date)));
            return l;
          },
          renderTile: (d) => RenderDemandTile(d),
          renderNone: () => Text("Vous n'avez créé aucune demande."),
        ));
  }
}
