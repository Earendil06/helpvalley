import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:helpvalley/models.dart';

class EditProfile extends StatefulWidget {
UserProfile user;

  EditProfile(this.user) ;

  @override
  _EditProfileState createState() => _EditProfileState();
}

class _EditProfileState extends State<EditProfile> {
  void updateInfos(BuildContext context) {
    if (widget.user.firstName.isEmpty ||
        widget.user.lastName.isEmpty ||
        widget.user.tel.isEmpty) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text("Erreur: Veuillez remplir tous les champs"),
      ));
      return;
    }

    FirebaseFirestore.instance
        .collection('users')
        .doc(FirebaseAuth.instance.currentUser.uid)
        .update({
      "firstName": widget.user.firstName,
      "lastName": widget.user.lastName,
      "tel": widget.user.tel,
      "displayTel": widget.user.displayTel
    }).then((value) => Get.back());
  }

  @override
  Widget build(context) {
    return Scaffold(
        appBar: AppBar(title: Text("Modifier mes informations")),
        body: Builder(
            builder: (contextBuilder) => Center(
                    child: Column(children: [
                  TextField(
                      controller:
                          new TextEditingController(text: widget.user
                              .firstName),
                      onChanged: (value) => this.widget.user.firstName = value,
                      decoration: InputDecoration(hintText: 'Prénom')),
                  TextField(
                      controller:
                          new TextEditingController(text: widget.user.lastName),
                      onChanged: (value) => this.widget.user.lastName = value,
                      decoration: InputDecoration(hintText: 'Nom')),
                  CheckboxListTile(
                    title: Text("Lorsque je demande de l'aide, je veux que "
                        "mon numéro s'affiche"),
                    value: widget.user.displayTel,
                    tristate: false,
                    onChanged: (newValue) {
                      setState(() {
                        widget.user.displayTel = newValue;
                      });
                    },
                    controlAffinity: ListTileControlAffinity.leading,
                  ),
                  TextButton(
                      onPressed: () => updateInfos(contextBuilder),
                      child: Text("Enregistrer"))
                ]))));
  }
}
