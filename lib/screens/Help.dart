import 'package:flutter/material.dart';
import 'package:helpvalley/components/Contact.dart';
import 'package:helpvalley/components/InfoRenderers.dart';

class ViewHelp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Informations")),
        body: SingleChildScrollView(
            child: Column(children: [
          InfoRendererWidget(
            title: "Contact",
            widget: Contact(),
          ),
          InfoRendererWidget(
              title: "Informations",
              widget: ToRead(images: [
                'assets/images/logocaveriviere.png',
                'assets/images/mcclic.png',
                'assets/images/laurence.jpg',
                'assets/images/croixrouge.jpg',
                'assets/images/dta.jpg',
              ], width: 3)),
        ])));
  }
}

class ToRead extends StatelessWidget {
  ToRead({this.images, this.width});

  List<String> images = [];
  int width = 3;

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Row(
            children: [
              Expanded(
                  child: Column(
                      children: getList1()
                          .map((imagePath) => Container(
                              padding: const EdgeInsets.only(bottom: 20),
                              child: Image.asset(
                                imagePath,
                                width: MediaQuery.of(context).size.width / 3,
                                height: MediaQuery.of(context).size.width / 3,
                              )))
                          .toList())),
              Expanded(
                  child: Column(
                children: getList2()
                    .map((imagePath) => Container(
                        padding: const EdgeInsets.only(bottom: 20),
                        child: Image.asset(
                          imagePath,
                          width: MediaQuery.of(context).size.width / 3,
                          height: MediaQuery.of(context).size.width / 3,
                        )))
                    .toList(),
              ))
            ],
          ),
          Padding(
            child: Text(
              "Bienvenue !",
              style: TextStyle(
                  fontSize: Theme.of(context).textTheme.headline6.fontSize),
            ),
            padding: const EdgeInsets.only(bottom: 10, top: 20),
          ),
          Padding(
            child: Text(
              "Cette application permet aux victimes de"
              " sinistres d'être mises en relation avec "
              "toutes personnes souhaitant leur venir en aide"
              " (bénévoles, élus, professionnels...) et "
              "d'organiser un réseau de solidarité.",
              textAlign: TextAlign.justify,
            ),
            padding: const EdgeInsets.only(bottom: 10),
          ),
          Padding(
            child: Text(
              "Les services proposés seront 100% gratuits, aucun "
              "démarchage commercial ne sera accepté !",
              textAlign: TextAlign.justify,
            ),
            padding: const EdgeInsets.only(bottom: 10),
          ),
          Padding(
            child: Text(
              "N'ajoutez pas de charges aux sinistrés, soyez un "
              "maximum autonome pour vos déplacements et "
              "logements.",
              textAlign: TextAlign.justify,
            ),
            padding: const EdgeInsets.only(bottom: 10),
          ),
          Text("Aidez et soutenez les vallées ! ...et MERCI ! 😘")
        ],
      ),
    );
  }

  List<String> getList1() {
    return this.images.sublist(0, (this.images.length / 2).floor());
  }

  List<String> getList2() {
    return this.images.sublist((this.images.length / 2).floor());
  }
}
