import 'dart:convert';

import "package:cloud_firestore/cloud_firestore.dart";
import "package:firebase_auth/firebase_auth.dart";
import "package:firebase_messaging/firebase_messaging.dart";
import "package:flutter/material.dart";
import "package:get/get.dart";
import "package:helpvalley/MessagingEngine.dart";
import 'package:helpvalley/RemoteConfigEngine.dart';
import "package:helpvalley/components/DatePicker.dart";
import "package:helpvalley/components/ListOfDemands.dart";
import "package:helpvalley/components/RenderDemandTile.dart";
import "package:helpvalley/screens/Cagnottes.dart";
import "package:helpvalley/screens/CreateDemand.dart";
import "package:helpvalley/screens/Help.dart";
import "package:helpvalley/screens/PastDemands.dart";
import "package:helpvalley/screens/profile/ViewProfile.dart";

class Home extends StatefulWidget {

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return DefaultTabController(
        length: RemoteConfigEngine.tabs().length,
        child: Scaffold(
            appBar: AppBar(
                title: Row(
                  children: [
                    Text("Demandes"),
                    Expanded(child: Text("")),
                    IconButton(
                        icon: Icon(Icons.person_rounded),
                        onPressed: () => Get.to(ViewProfile())),
                    IconButton(
                        icon: Icon(Icons.help_outline),
                        onPressed: () => Get.to(ViewHelp())),
                    IconButton(
                        icon: Icon(Icons.archive),
                        onPressed: () => Get.to(PastDemands())),
                  ],
                ),
                bottom: TabBar(
                  tabs: RemoteConfigEngine.tabs().map(
                          (e) => Tab(text: e.replaceAll("Vallée", "").trim
                            ()))
                      .toList(),
                )),
            body: TabBarView(
                children:
                RemoteConfigEngine.tabs().map((e) => ListDemands(e)).toList()),
            floatingActionButton: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Padding(
                      padding: const EdgeInsets.only(bottom: 8.0),
                      child: FloatingActionButton.extended(
                          heroTag: "btn1",
                          label: Text("Dons"),
                          icon: Icon(Icons.monetization_on_rounded),
                          onPressed: () => Get.to(Donations()))),
                  FloatingActionButton.extended(
                      heroTag: "btn2",
                      label: Text("J'ai besoin d'aide"),
                      icon: Icon(Icons.add),
                      onPressed: () => {Get.to(CreateDemand())})
                ])));
  }
}

class ListDemands extends StatelessWidget {
  String valley;

  ListDemands(this.valley);

  @override
  Widget build(BuildContext context) {
    Query users = FirebaseFirestore.instance
        .collection('demands')
        .where("place", isEqualTo: valley)
        .where("canceled", isEqualTo: false);

    return
      ListOfDemands(
        query: users,
        processList: (ll) {
          var l = ll
              .where(
                  (element) => element.isFutureOrToday())
              .toList();
          l.sort((a, b) =>
              MyTextFieldDatePicker.parseDate(a.date)
                  .compareTo(MyTextFieldDatePicker.parseDate(b.date)));
          return l;
        },
        renderTile: (d) => RenderDemandTile(d),
        renderNone: () => Text("Il n'y a pas de demandes pour le moment.")
      );
  }
}


