import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:helpvalley/components/Contact.dart';
import 'package:helpvalley/components/InfoRenderers.dart';
import 'package:helpvalley/screens/public/Registration.dart';
import 'package:helpvalley/screens/public/Reset.dart';

class Login extends StatelessWidget {
  void login(context) {
    FirebaseAuth.instance
        .signInWithEmailAndPassword(
            email: this.email.trim(), password: this.password.trim())
        .then((value) => {}, onError: (error) {
      FirebaseAuthException e = error as FirebaseAuthException;
      String text = '';
      print(e.code);
      if (e.code == "wrong-password") {
        text = "Erreur: utilisateur ou mot de passe incorrect";
      }
      if (e.code == "user-not-found") {
        text = "Erreur: utilisateur ou mot de passe incorrect";
      }
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(text),
      ));
    });
  }

  String email;
  String password;

  @override
  Widget build(context) {
    return Scaffold(
        appBar: AppBar(title: Text("Se connecter")),
        body: Builder(
            builder: (buildContext) => Center(
                    child: Column(children: [
                  InfoRendererWidget(
                      title: "Email",
                      widget: TextField(
                          enableSuggestions: false,
                          autocorrect: false,
                          keyboardType: TextInputType.emailAddress,
                          onChanged: (value) => this.email = value,
                          decoration: InputDecoration(hintText: 'Email'))),
                  InfoRendererWidget(
                      title: "Mot de passe",
                      widget: TextField(
                          enableSuggestions: false,
                          autocorrect: false,
                          obscureText: true,
                          onChanged: (value) => this.password = value,
                          decoration:
                              InputDecoration(hintText: 'Mot de passe'))),
                  TextButton(
                      onPressed: () => login(buildContext),
                      child: Text("Se "
                          "connecter")),
                  Text("ou"),
                  TextButton(
                      onPressed: () => Get.to(Registration()),
                      child: Text("Créer un compte")),
                  TextButton(
                    child: Text("Mot de passe oublié?",
                        style: TextStyle(
                            color: Colors.blue, fontStyle: FontStyle.italic)),
                    onPressed: () {
                      Get.to(Reset());
                    },
                  ),
                  Expanded(
                    child: Text(""),
                  ),
                  InfoRendererWidget(
                    title: "Contact",
                    widget: Contact(),
                  )
                  // TextButton(onPressed: signInWithGoogle, child: Text("login google"))
                ]))));
  }
}
