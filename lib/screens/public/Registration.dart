import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:helpvalley/components/InfoRenderers.dart';
import 'package:helpvalley/main.dart';
import 'package:url_launcher/url_launcher.dart';

class Registration extends StatefulWidget {
  @override
  _RegistrationState createState() => _RegistrationState();
}

class _RegistrationState extends State<Registration> {
  String email;

  String password;

  String confirmation;

  String firstName;

  String lastName;

  // String tel;

  bool acceptConditions = false;

  void create(BuildContext context) {
    if (email.isNullOrBlank ||
            password.isNullOrBlank ||
            confirmation.isNullOrBlank ||
            firstName.isNullOrBlank ||
            lastName.isNullOrBlank
        // ||
        // tel.isNullOrBlank
        ) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text("Erreur: Veuillez remplir tous les champs"),
      ));
      return;
    }

    if (password.trim() != confirmation.trim()) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text("Erreur: Les mots de passe de correspondent pas"),
      ));
      return;
    }

    FirebaseAuth.instance
        .createUserWithEmailAndPassword(email: email.trim(), password:
    password.trim())
        .then((emailUserCredentials) {
      FirebaseFirestore.instance
          .collection('users')
          .doc(emailUserCredentials.user.uid)
          .set({
        "email": email.trim(),
        "firstName": firstName,
        "lastName": lastName,
        "displayTel": false
      });
    }, onError: (error) {
      FirebaseAuthException e = error as FirebaseAuthException;
      String text = '';
      print(e.code);
      if (e.code == "invalid-email") {
        text = "Erreur: l'email n'a pas le bon format";
      }
      if (e.code == "weak-password") {
        text = "Erreur: Le mot de passe est trop faible, 6 caractères minimum.";
      }
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text(text),
      ));
      // print("e="+error.toString());
    });
  }

  @override
  Widget build(context) {
    return Scaffold(
        appBar: AppBar(title: Text("Inscription")),
        body: Builder(
            builder: (contextBuilder) => SingleChildScrollView(
                    child: Center(
                        child: Column(children: [
                  InfoRendererWidget(
                      title: "Email",
                      widget: TextField(
                          enableSuggestions: false,
                          autocorrect: false,
                          keyboardType: TextInputType.emailAddress,
                          onChanged: (value) => this.email = value,
                          decoration: InputDecoration(hintText: ''))),
                  InfoRendererWidget(
                      title: "Mot de passe",
                      widget: TextField(
                          enableSuggestions: false,
                          autocorrect: false,
                          obscureText: true,
                          onChanged: (value) => this.password = value,
                          decoration: InputDecoration(hintText: ''))),
                  InfoRendererWidget(
                      title: "Confirmer le mot de passe",
                      widget: TextField(
                          enableSuggestions: false,
                          autocorrect: false,
                          obscureText: true,
                          onChanged: (value) => this.confirmation = value,
                          decoration: InputDecoration(hintText: ''))),
                  InfoRendererWidget(
                      title: "Prénom",
                      widget: TextField(
                          onChanged: (value) => this.firstName = value,
                          decoration: InputDecoration(hintText: ''))),
                  InfoRendererWidget(
                      title: "Nom",
                      widget: TextField(
                          onChanged: (value) => this.lastName = value,
                          decoration: InputDecoration(hintText: ''))),
                  Container(
                    child: TextButton(
                      child: Text(
                          "Cliquez ici pour lire les conditions "
                          "d'utilisation",
                          style: TextStyle(
                              color: Colors.blue, fontStyle: FontStyle.italic)),
                      onPressed: () async {
                        var url = "https://www.aide1vallee.fr/CGU";
                        if (await canLaunch(url)) {
                          await launch(url);
                        } else {
                          throw 'Could not launch $url';
                        }
                      },
                    ),
                    padding: const EdgeInsets.only(top: 20, bottom: 20),
                  ),
                  CheckboxListTile(
                    title: Text("En cochant cette case, vous certifiez que "
                        "vous avez lu et accepte les conditions d'utilisation"
                        " de l'application"),
                    value: acceptConditions,
                    tristate: false,
                    onChanged: (newValue) {
                      setState(() {
                        acceptConditions = newValue;
                      });
                    },
                    controlAffinity: ListTileControlAffinity.leading,
                  ),
                  TextButton(
                      onPressed: acceptConditions
                          ? () => create(contextBuilder)
                          : null,
                      child: Text("Créer le "
                          "compte"))
                ])))));
  }
}
