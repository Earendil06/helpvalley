import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:helpvalley/RoutingEngine.dart';
import 'package:helpvalley/components/InfoRenderers.dart';

class Verify extends StatefulWidget {
  @override
  _VerifyState createState() => _VerifyState();
}

class _VerifyState extends State<Verify> {
  String tel;

  String verificationId;

  String sms = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Validation du numéro")),
      body: Column(
        children: [
          Visibility(
              visible: verificationId.isNullOrBlank,
              child: InfoRendererWidget(
                title: "Numéro de téléphone",
                widget: TextField(
                    // controller: TextEditingController(text: tel),
                    keyboardType: TextInputType.number,
                    inputFormatters: <TextInputFormatter>[
                      FilteringTextInputFormatter.digitsOnly
                    ],
                    onChanged: (value) => setState(() {
                          tel = value;
                        }),
                    decoration: InputDecoration(hintText: 'Téléphone')),
              )),
          Visibility(
              visible: !verificationId.isNullOrBlank,
              child: InfoRendererWidget(
                title: "Saisir le code",
                widget: TextField(
                    keyboardType: TextInputType.number,
                    inputFormatters: <TextInputFormatter>[
                      FilteringTextInputFormatter.digitsOnly
                    ],
                    onChanged: (value) => setState(() {
                          sms = value;
                        }),
                    decoration: InputDecoration(hintText: 'Code')),
              )),
          Visibility(
              visible: !verificationId.isNullOrBlank,
              child: TextButton(
                  onPressed: sms.isNullOrBlank
                      ? null
                      : () {
                          FirebaseAuth.instance.currentUser
                              .linkWithCredential(PhoneAuthProvider.credential(
                                  verificationId: verificationId, smsCode: sms))
                              .then((value) {
                            FirebaseFirestore.instance
                                .collection("users")
                                .doc(FirebaseAuth.instance.currentUser.uid)
                                .update({"tel": tel});
                            RoutingEngine().routeUserToPage(
                                FirebaseAuth.instance.currentUser);
                          });
                        },
                  child: Text("Valider"))),
          Visibility(
              visible: verificationId.isNullOrBlank,
              child: TextButton(
                  onPressed: tel.isNullOrBlank
                      ? null
                      : () {
                          FirebaseAuth.instance.verifyPhoneNumber(
                              phoneNumber: formatTel(tel),
                              verificationCompleted: (phoneAuthCredential) =>
                                  {},
                              verificationFailed: (error) => {print(error)},
                              codeSent: (verificationId, forceResendingToken) {
                                setState(() {
                                  this.verificationId = verificationId;
                                });
                              },
                              codeAutoRetrievalTimeout: (verificationId) => {});
                        },
                  child: Text("Recevoir le code"))),
          Expanded(child: Text("")),
          new TextButton(
              onPressed: () => FirebaseAuth.instance.signOut(),
              child: new Text("Se déconnecter"))
        ],
      ),
    );
  }

  String formatTel(String tel) {
    var s = tel.removeAllWhitespace.replaceAll(".", "").replaceAll("-", "");
    return "+33" + s;
  }
}
