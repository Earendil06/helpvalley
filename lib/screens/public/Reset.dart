import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:helpvalley/components/InfoRenderers.dart';
import 'package:helpvalley/screens/public/Login.dart';

class Reset extends StatefulWidget {
  @override
  _ResetState createState() => _ResetState();
}

class _ResetState extends State<Reset> {
  String email;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Se connecter")),
        body: Builder(
            builder: (buildContext) => Center(
                child: Column(children: [
                  InfoRendererWidget(
                      title: "Email",
                      widget: TextField(
                          onChanged: (value) => setState(() {
                            this.email = value;
                          }),
                          decoration: InputDecoration(hintText: 'Email'))),
                  Text("Un email contenant les instructions de "
                      "réinitialisation vous sera envoyé"),
                  TextButton(
                      onPressed: () => FirebaseAuth.instance
                          .sendPasswordResetEmail(email: this.email.trim())
                          .then((value) => Get.offAll(Login())),
                      child: Text("Réinitialiser le mot de passe")),
                  // TextButton(onPressed: signInWithGoogle, child: Text("login google"))
                ]))));
  }
}
