
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:helpvalley/RemoteConfigEngine.dart';
import 'package:helpvalley/UIUtils.dart';
import 'package:helpvalley/components/DatePicker.dart';
import 'package:helpvalley/components/DropDownWidget.dart';
import 'package:helpvalley/components/InfoRenderers.dart';
import 'package:helpvalley/models.dart';
import 'package:helpvalley/screens/Home.dart';

class CreateDemand extends StatefulWidget {
  Demand demand = Demand()
    ..userID = FirebaseAuth.instance.currentUser.uid;

  CreateDemand.edit(this.demand, this.demandID) {
    state = _EditDemandState();
  }

  CreateDemand();

  String demandID;
  AbstractState state = _CreateDemandState();

  @override
  AbstractState createState() => state;
}

class _EditDemandState extends AbstractState {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
            title: PreferredSize(
                preferredSize: Size.fromHeight(kToolbarHeight),
                child: Builder(
                  builder: (contextBuilder) => Row(children: [
                    Expanded(child: Text("Modifier la demande")),
                    IconButton(
                        onPressed: () => save(contextBuilder),
                        icon: Icon(Icons.check))
                  ]),
                ))),
        body: DemandEditor(widget.demand));
  }

  @override
  void exportToDatabase() {
    FirebaseFirestore.instance
        .collection('demands')
        .doc(widget.demandID)
        .update({
      "title": widget.demand.title,
      "information": widget.demand.information,
      "place": widget.demand.place,
      "date": widget.demand.date,
      "duration": widget.demand.duration,
      "numberRequired": widget.demand.numberRequired,
      "material": widget.demand.material,
      "summary": widget.demand.summary,
      "village": widget.demand.village,
      "hardAccess": widget.demand.hardAccess,
    }).then((value) => {Get.back()});
  }
}

abstract class AbstractState extends State<CreateDemand> {
  void exportToDatabase();

  void save(BuildContext context) {
    if (this.widget.demand.title.isNullOrBlank ||
        this.widget.demand.village.isNullOrBlank ||
        this.widget.demand.information.isNullOrBlank ||
        this.widget.demand.place.isNullOrBlank ||
        this.widget.demand.date.isNullOrBlank ||
        this.widget.demand.duration.isNullOrBlank ||
        this.widget.demand.numberRequired.isNullOrBlank ||
        this.widget.demand.numberRequired == 0 ||
        this.widget.demand.summary.isNullOrBlank) {
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text("Erreur: Veuillez remplir tous les champs"),
      ));
      return;
    }

    if (possibleWrongDate()) {
      showAlertDialog(context,
          title: Text("Vérifier la date"),
          body: Text("La date choisie est: " +
              this.widget.demand.date +
              ". Pensez à"
                  " laisser un délais raisonnable pour que les bénévoles puissent "
                  "voir votre demande et s'y inscrire. Après le " +
              this.widget.demand.date +
              ", votre demande n'apparaîtra plus dans la liste."),
          cancelText: "Je corrige la date",
          acceptText: "Tout est bon, je valide",
          onAccept: exportToDatabase);
    } else {
      exportToDatabase();
    }
  }

  bool possibleWrongDate() {
    DateTime d = MyTextFieldDatePicker.parseDate(widget.demand.date);
    DateTime today = DateTime.now();
    int days = DateTimeRange(start: today, end: d).duration.inDays;
    return days <= 1;
  }
}

class _CreateDemandState extends AbstractState {
  @override
  Widget build(context) {
    return Scaffold(
        resizeToAvoidBottomInset: true,
        appBar: AppBar(
            title: PreferredSize(
                preferredSize: Size.fromHeight(kToolbarHeight),
                child: Builder(
                  builder: (contextBuilder) => Row(children: [
                    Expanded(child: Text("Créer une demande")),
                    IconButton(
                        onPressed: () => save(contextBuilder),
                        icon: Icon(Icons.check))
                  ]),
                ))),
        body: DemandEditor(widget.demand));
  }

  @override
  void exportToDatabase() {
    FirebaseFirestore.instance
        .collection('demands')
        .add(widget.demand.toFirestoreModel())
        .then((value) => Get.offAll(Home()));
  }
}

class DemandEditor extends StatefulWidget {
  Demand demand;

  DemandEditor(this.demand);

  @override
  _DemandEditorState createState() => _DemandEditorState();
}

class _DemandEditorState extends State<DemandEditor> {
  List<String> getTypesOrdered() {
    List<String> categories = RemoteConfigEngine.categories();
    categories.sort();
    return categories;
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Builder(
            builder: (contextBuilder) => new SingleChildScrollView(
                    child: Column(children: [
                  InfoRendererWidget(
                      title: "Type de demande",
                      widget: DropdownWidget(
                          hintText: "",
                          title: "",
                          currentItem: widget.demand.title,
                          items: getTypesOrdered(),
                          itemCallBack: (value) =>
                              widget.demand.title = value)),
                  InfoRendererWidget(
                      title: "Titre de la demande",
                      widget: TextField(
                          maxLength: 40,
                          maxLengthEnforced: true,
                          controller: new TextEditingController(
                              text: widget.demand.summary),
                          onChanged: (value) {
                            if (value.length <= 40) {
                              widget.demand.summary = value;
                            }
                          },
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(40),
                          ],
                          decoration: InputDecoration(hintText: ""))),
                  InfoRendererWidget(
                      title: "Besoin matériel",
                      widget: TextField(
                          keyboardType: TextInputType.multiline,
                          minLines: 2,
                          maxLines: 8,
                          controller: new TextEditingController(
                              text: widget.demand.material),
                          onChanged: (value) => widget.demand.material = value,
                          decoration: InputDecoration(
                              hintText:
                                  "Veuillez renseigner ici le matériel souhaité"))),
                  InfoRendererWidget(
                      title: "Détails de la demande",
                      widget: TextField(
                          keyboardType: TextInputType.multiline,
                          minLines: 3,
                          maxLines: 8,
                          controller: new TextEditingController(
                              text: widget.demand.information),
                          onChanged: (value) =>
                              widget.demand.information = value,
                          decoration: InputDecoration(
                              hintText:
                                  "Essayez de détailler un maximum le besoin, les informations utiles pour les volontaires, adresse, etc."))),
                  InfoRendererWidget(
                      title: "Date",
                      widget: MyTextFieldDatePicker(
                          labelText: "",
                          dateFormat: MyTextFieldDatePicker.myDateFormat,
                          initialDate: widget.demand.date.isNull
                              ? DateTime.now()
                              : MyTextFieldDatePicker.parseDate(
                                  widget.demand.date),
                          firstDate: DateTime.now(),
                          lastDate: DateTime.now().add(new Duration(days: 365)),
                          onDateChanged: (v) => {
                                widget.demand.date =
                                    MyTextFieldDatePicker.toFormattedString(v)
                              })),
                  InfoRendererWidget(
                      title: "Vallée concernée",
                      widget: DropdownWidget(
                          hintText: "",
                          title: "",
                          currentItem: widget.demand.place,
                          items: RemoteConfigEngine.tabs().toList(),
                          itemCallBack: (value) =>
                              widget.demand.place = value)),
                  InfoRendererWidget(
                      title: "Village concerné",
                      widget: TextField(
                          controller: new TextEditingController(
                              text: widget.demand.village),
                          onChanged: (value) => widget.demand.village = value,
                          decoration: InputDecoration(hintText: ""))),
                  InfoRendererWidget(
                      title: "Nombre de personnes souhaitées",
                      widget: TextField(
                          controller: TextEditingController(
                              text: widget.demand.numberRequired.toString()),
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          onChanged: (value) =>
                              widget.demand.numberRequired = int.parse(value),
                          decoration:
                              InputDecoration(labelText: "", hintText: ""))),
                  InfoRendererWidget(
                      title: "Durée approximative (en heures)",
                      widget: TextField(
                          controller: new TextEditingController(
                              text: widget.demand.duration.toString()),
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          onChanged: (value) =>
                              this.widget.demand.duration = int.parse(value),
                          decoration:
                              InputDecoration(labelText: "", hintText: ''))),
                  CheckboxListTile(
                    title: Text("Accès difficile ?"),
                    value: widget.demand.hardAccess,
                    tristate: false,
                    onChanged: (newValue) {
                      setState(() {
                        widget.demand.hardAccess = newValue;
                      });
                    },
                    controlAffinity: ListTileControlAffinity.leading,
                  ),
                ]))));
  }
}
