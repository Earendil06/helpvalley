import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:helpvalley/RemoteConfigEngine.dart';
import 'package:helpvalley/components/DatePicker.dart';
import 'package:helpvalley/components/ListOfDemands.dart';
import 'package:helpvalley/components/RenderDemandTile.dart';
import 'package:helpvalley/screens/CreateDemand.dart';

class PastDemands extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: RemoteConfigEngine.tabs().length,
        child: Scaffold(
          appBar: AppBar(
              title: Text("Demandes passées"),
              bottom: TabBar(
                tabs: RemoteConfigEngine.tabs().map(
                        (e) => Tab(text: e.replaceAll("Vallée", "").trim()))
                    .toList(),
              )),
          body: TabBarView(
              children:
              RemoteConfigEngine.tabs().map((e) => ListDemandsPast(e)).toList()),
        ));
  }
}

class ListDemandsPast extends StatelessWidget {
  String valley;

  ListDemandsPast(this.valley);

  @override
  Widget build(BuildContext context) {
    Query oldDemands = FirebaseFirestore.instance
        .collection('demands')
        .where("place", isEqualTo: valley);

    return ListOfDemands(
      query: oldDemands,
      renderNone: () => Text("Pas de demandes passées pour cette vallée."),
      renderTile: (d) => RenderDemandTile(d),
      processList: (ll) {
        var l = ll.where((element) => element.isPastOrArchived()).toList();
        l.sort((a, b) => MyTextFieldDatePicker.parseDate(b.date)
            .compareTo(MyTextFieldDatePicker.parseDate(a.date)));
        return l;
      },
    );
  }
}
