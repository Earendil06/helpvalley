import 'dart:io';
import 'dart:typed_data';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:helpvalley/DynamicLinkService.dart';
import 'package:helpvalley/PdfGenerator.dart';
import 'package:helpvalley/components/InfoRenderers.dart';
import 'package:helpvalley/models.dart';
import 'package:helpvalley/screens/CreateDemand.dart';
import 'package:helpvalley/screens/Home.dart';
import 'package:open_file/open_file.dart';
import 'package:path_provider/path_provider.dart';
import 'package:pdf/pdf.dart';
import 'package:share/share.dart';
import 'package:url_launcher/url_launcher.dart';

class ViewDemand extends StatelessWidget {
  final String demandId;

  final List<dynamic> list = [];

  ViewDemand(this.demandId);

  void cancel() {
    FirebaseFirestore.instance.collection('demands').doc(this.demandId).update({
      "canceled": true,
    });
  }

  void reopen() {
    FirebaseFirestore.instance.collection('demands').doc(this.demandId).update({
      "canceled": false,
    });
  }

  Future<Uint8List> getPdfDoc(Demand demand) async {
    var people = await FirebaseFirestore.instance
        .collection("users")
        .where(FieldPath.documentId,
            whereIn: demand.people.followedBy(["FAKEID"]).toList())
        .get();

    var peopleData = people.docs.map((e) => UserProfile(e)).toList();

    var r = await generatePdf(demand, peopleData, PdfPageFormat.a4);
    return r;
  }

  @override
  Widget build(context) {
    DocumentReference demand =
        FirebaseFirestore.instance.collection('demands').doc(demandId);

    return new WillPopScope(
        onWillPop: () async => Get.offAll(Home()),
        child: Scaffold(
            appBar: AppBar(
                title: Row(
              children: [
                Text("Résumé de la demande"),
                Expanded(child: Text("")),
                IconButton(
                    icon: Icon(Icons.share),
                    onPressed: () async {
                      var uri = await DynamicLinkService()
                          .createDynamicLink(demandId);
                      Share.share(uri.shortUrl.toString());
                    }),
              ],
            )),
            body: StreamBuilder(
              stream: demand.snapshots(),
              builder: (BuildContext context,
                  AsyncSnapshot<DocumentSnapshot> snapshot) {
                if (!snapshot.hasData) {
                  return Text("Loading");
                }
                var document = Demand.fromFirestore(snapshot.data);
                return SingleChildScrollView(
                    child: Column(children: [
                  Row(
                      children: listActions(
                          FirebaseAuth.instance.currentUser.uid ==
                              document.userID,
                          document,
                          demand)),
                  InfoRendererText(
                      title: "Type de demande", text: document.title),
                  InfoRendererText(title: "Mission", text: document.summary),
                  InfoRendererText(title: "Village", text: document.village),
                  InfoRendererText(
                      title: "Détails", text: document.information),
                  InfoRendererText(
                      title: "Besoins matériels", text: document.material),
                  InfoRendererText(
                      title: "Date",
                      text: "${document.date}" +
                          (document.isFutureOrToday()
                              ? " (dans ${document.computeJ()} "
                                  "jours)"
                              : ""
                                  "")),
                  InfoRendererText(
                      title: "Durée de la mission",
                      text: document.duration.toString() + "h"),
                  InfoRendererWidget(
                      title: "Auteur de la demande",
                      widget: FutureBuilder<DocumentSnapshot>(
                        future: FirebaseFirestore.instance
                            .collection("users")
                            .doc(document.userID)
                            .get(),
                        builder: (BuildContext context,
                            AsyncSnapshot<DocumentSnapshot> snapshot) {
                          if (snapshot.hasError) {
                            return ListTile(
                              title: Text("Loading"),
                            );
                          }

                          if (snapshot.connectionState ==
                              ConnectionState.done) {
                            UserProfile profile = UserProfile(snapshot.data);
                            return Row(
                              children: [
                                Text(
                                    profile.firstName + " " + profile.lastName),
                                Expanded(child: Text("")),
                                profile.displayTel
                                    ? TextButton(
                                        onPressed: () =>
                                            launch("tel://" + profile.tel),
                                        child: Text("Appeler"))
                                    : Text("")
                              ],
                            );
                          }

                          return Text("loading");
                        },
                      )),
                  InfoRendererText(
                      title: "Inscrits",
                      text: "${document.people.length.toString()} / "
                          "${document.numberRequired.toString()}"),
                  InfoRendererWidget(
                      title: "Bénévoles inscrits",
                      additionalTitleWidgets: document.people.length > 0
                          ? [
                              IconButton(
                                  icon: Icon(Icons.print),
                                  iconSize: 18,
                                  padding: EdgeInsets.only(left: 20),
                                  color: Colors.purple,
                                  onPressed: () async {
                                    final directory =
                                        await getApplicationDocumentsDirectory();
                                    final path =
                                        '${directory.path}/liste_benevoles.pdf';
                                    final file = File(path);
                                    final pdf = await getPdfDoc(document);
                                    // final file = File.fromRawPath(pdf);
                                    file.writeAsBytesSync(pdf);
                                    OpenFile.open(path);
                                    // Share.shareFiles([path], text: csv);
                                  }),
                            ]
                          : [],
                      widget: ListView(
                          physics: NeverScrollableScrollPhysics(),
                          scrollDirection: Axis.vertical,
                          shrinkWrap: true,
                          children: document.people
                              .map((item) => item as String)
                              .toList()
                              .map((String userID) {
                            return FutureBuilder<DocumentSnapshot>(
                              future: FirebaseFirestore.instance
                                  .collection("users")
                                  .doc(userID)
                                  .get(),
                              builder: (BuildContext context,
                                  AsyncSnapshot<DocumentSnapshot> snapshot) {
                                if (snapshot.hasError) {
                                  return ListTile(
                                    title: Text("Loading"),
                                  );
                                }

                                if (snapshot.connectionState ==
                                    ConnectionState.done) {
                                  UserProfile profile =
                                      UserProfile(snapshot.data);
                                  return ListTile(
                                    title: Row(children: [
                                      Text(
                                          "${profile.firstName} ${profile.lastName}"),
                                      Expanded(child: Text("")),
                                      TextButton(
                                          onPressed: () =>
                                              launch("tel://" + profile.tel),
                                          child: Text("Appeler")),
                                    ]),
                                  );
                                }

                                return Text("loading");
                              },
                            );
                          }).toList()))
                ]));
              },
            )));
  }

  List<Widget> listActions(
      bool isItMe, Demand document, DocumentReference demand) {
    List<Widget> l = [];
    if (isItMe) {
      if (document.canceled) {
        l.add(TextButton(
            onPressed: () => reopen(), child: Text("Réouvrir la demande")));
      } else {
        l.add(TextButton(
            onPressed: () => cancel(), child: Text("Annuler la demande")));

        l.add(TextButton(
            onPressed: () => {Get.to(CreateDemand.edit(document, demandId))},
            child: Text("Modifier la demande")));
      }
    } else if (!(document.canceled)) {
      int overbooking = 3;
      if (document.people.contains(FirebaseAuth.instance.currentUser.uid)) {
        l.add(TextButton(
            onPressed: () => {
                  demand.update({
                    "people": FieldValue.arrayRemove(
                        [FirebaseAuth.instance.currentUser.uid])
                  })
                },
            child: Text("Je me désiste")));
      } else {
        if (document.people.length <= document.numberRequired + overbooking) {
          l.add(TextButton(
              onPressed: () => {
                    demand.update({
                      "people": FieldValue.arrayUnion(
                          [FirebaseAuth.instance.currentUser.uid])
                    })
                  },
              child: Text("Je veux aider!")));
        } else {
          l.add(Chip(
              backgroundColor: Colors.green,
              label: Text("Nombre de bénévoles inscrits atteint!")));
        }
      }
    }
    return l;
  }
}
