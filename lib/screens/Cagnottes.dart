import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:helpvalley/components/FirebaseList.dart';
import 'package:helpvalley/models.dart';
import 'package:url_launcher/url_launcher.dart';

class Donations extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    CollectionReference donations =
        FirebaseFirestore.instance.collection('leetchi');

    return Scaffold(
        appBar: AppBar(
            title: Row(
          children: [
            Text("Liste des cagnottes"),
          ],
        )),
        body: FirebaseList<Donation>(
          query: donations,
          convert: (e) => Donation(e),
          listElementWidget: (Donation e) => ListTile(
            title: Text(e.name),
            onTap: () async {
              var url = e.link;
              if (await canLaunch(url)) {
                await launch(url);
              } else {
                throw 'Could not launch $url';
              }
            },
          ),
        ));
  }
}
