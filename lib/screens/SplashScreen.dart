import 'package:firebase_remote_config/firebase_remote_config.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:helpvalley/screens/Help.dart';
import 'package:helpvalley/screens/Home.dart';

class SplashScreen extends StatelessWidget {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: Text("Participent à l'effort")),
        body: GestureDetector(
            onTap: () => Get.offAll(Home()),
            child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                      child: Padding(
                    child: ToRead(images:
                    [
                      "assets/images/azur.jpg",
                      "assets/images/courtage.jpg",
                      "assets/images/trek.jpg",
                      "assets/images/mlle.jpg"
                    ]),
                    padding:
                        const EdgeInsets.only(top: 20, left: 20, right: 20),
                  )),
                  Container(
                      padding: const EdgeInsets.only(bottom: 40),
                      child: TextButton(
                          child: Text(
                            "Fermer",
                            style: TextStyle(fontSize: 25),
                          ),
                          onPressed: () => Get.offAll(Home())))
                ])));
  }
}
