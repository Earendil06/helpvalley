import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:helpvalley/RemoteConfigEngine.dart';

class DynamicLinkService {
  Future<ShortDynamicLink> createDynamicLink(String demandId) async {
    final DynamicLinkParameters parameters = DynamicLinkParameters(
      uriPrefix: '$urlPrefix',
      link: Uri.parse(getDemandLink(demandId)),
      androidParameters: AndroidParameters(
        packageName: 'com.helpvalley',
        minimumVersion: 0,
      ),
      dynamicLinkParametersOptions: DynamicLinkParametersOptions(
        shortDynamicLinkPathLength: ShortDynamicLinkPathLength.short,
      ),
      iosParameters: IosParameters(
        bundleId: 'com.helpvalley.fr',
        minimumVersion: '1.0',
        appStoreId: '1539612738',
      ),
    );

    final ShortDynamicLink shortLink = await parameters.buildShortLink();
    return shortLink;
  }

  String getDemandLink(String demandId) {
    return '$urlPrefix/$demandId';
  }

  String get urlPrefix =>
      RemoteConfigEngine.instance.getString("url_prefix_deeplink");
}
