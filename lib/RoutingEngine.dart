import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_dynamic_links/firebase_dynamic_links.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:helpvalley/MessagingEngine.dart';
import 'package:helpvalley/screens/Home.dart';
import 'package:helpvalley/screens/SplashScreen.dart';
import 'package:helpvalley/screens/ViewDemand.dart';
import 'package:helpvalley/screens/profile/ViewProfile.dart';
import 'package:helpvalley/screens/public/Login.dart';
import 'package:helpvalley/screens/public/Verify.dart';

class RoutingEngine extends StatelessWidget {
  void routeUserToPage(User user, {PendingDynamicLinkData data}) async {
    if (user.isNull) {
      print("user disconnected: GOTO login");
      Get.offAll(Login());
    } else {
      String userID = FirebaseAuth.instance.currentUser.uid;
      FirebaseMessaging.instance
          .getToken()
          .then((value) => MessagingEngine.saveTokenToDatabase(userID, value));
      FirebaseMessaging.instance.onTokenRefresh.listen(
          (token) => MessagingEngine.saveTokenToDatabase(userID, token));
      if (user.phoneNumber.isNullOrBlank) {
        print("User connected but not verified: GOTO verification");
        Get.offAll(Verify());
      } else {
        if (data == null) {
          data = await FirebaseDynamicLinks.instance.getInitialLink();
        }
        final Uri deepLink = data?.link;
        if (deepLink != null) {
          print("user connected and verified and deep link: GOTO deeplink "
              "page");
          Get.to(ViewDemand(deepLink.path.toString().substring(1)));
        } else {
          print("user connected and verified: GOTO sponsors");
          Get.offAll(SplashScreen());
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    FirebaseAuth.instance
        .authStateChanges()
        .listen((user) => routeUserToPage(user));
    FirebaseAuth.instance.currentUser?.reload();
    print("Processing route: display black screen");

    return Scaffold(backgroundColor: Colors.black, body: Text("Loading"));
  }
}
