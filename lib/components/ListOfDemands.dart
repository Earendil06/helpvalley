import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:helpvalley/models.dart';

class ListOfDemands extends StatelessWidget {
  Query query;

  final List<Demand> Function(List<Demand>) processList;

  final Widget Function(Demand) renderTile;

  final Widget Function() renderNone;

  ListOfDemands(
      {this.query, this.processList, this.renderTile, this.renderNone});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: query.snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text('Something went wrong');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Text("Loading");
        }

        var l = processList(
            snapshot.data.docs.map((e) => Demand.fromFirestore(e)).toList());

        if (l.isNotEmpty) {
          return new ListView.separated(
            separatorBuilder: (context, index) => Divider(
              color: Color(0x33000000),
            ),
            itemBuilder: (BuildContext context, int index) {
              return renderTile(l[index]);
            },
            itemCount: l.length,
          );
        } else {
          return renderNone();
        }
      },
    );
  }
}
