
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class InfoRendererWidget extends StatelessWidget {
  String title;
  Widget widget;

  Iterable<Widget> additionalTitleWidgets = [];

  InfoRendererWidget({this.title, this.widget, this.additionalTitleWidgets});

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            border:
            Border.all(width: 1.0, color: Theme.of(context).dividerColor),
            borderRadius: BorderRadius.all(Radius.circular(5.0))),
        margin: const EdgeInsets.all(5),
        padding: const EdgeInsets.all(10),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(children: <Widget>[Text(this.title, style: TextStyle
              (fontWeight: FontWeight.bold),)].followedBy
              (additionalTitleWidgets).toList()),
            Divider(),
            this.widget,
          ],
        ));
  }
}

class InfoRendererText extends StatelessWidget {
  String title;
  String text;

  InfoRendererText({this.title, this.text});

  @override
  Widget build(BuildContext context) {

    return InfoRendererWidget(
        title: title,
        widget: Text(this.text.isNullOrBlank ? "": this.text, textAlign:
        TextAlign.justify,
            style: TextStyle(
              // fontStyle: FontStyle.italic,
                fontSize: Theme.of(context).textTheme.bodyText1.fontSize)));
  }
}
