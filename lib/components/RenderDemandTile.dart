import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:helpvalley/models.dart';
import 'package:helpvalley/screens/ViewDemand.dart';

class RenderDemandTile extends StatelessWidget {
  Demand demand;

  RenderDemandTile(this.demand);

  @override
  Widget build(BuildContext context) {
    var numberRequired = demand.numberRequired;
    var numberPeople = demand.people.length;
    var official = demand.official;
    var officialAide1Vallee = demand.officialAide1Vallee;
    var nbJBefore = demand.computeJ();
    var textJ = nbJBefore == 0 ? "AUJOURD'HUI" : "DANS ${nbJBefore}j";
    if (nbJBefore < 0) {
      textJ = "";
    }
    return new ListTile(
      tileColor: officialAide1Vallee
          ? Color(0xFBF5E8aa)
          : (official ? Color(0xFFcdf7f7) : Colors.white),
      title: Column(children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Container(
                padding: const EdgeInsets.only(top: 3),
                decoration: BoxDecoration(
                    border: Border(
                        bottom:
                            BorderSide(color: Theme.of(context).dividerColor))),
                child: Row(
                  children: [
                    official || officialAide1Vallee
                        ? Icon(
                            Icons.star,
                            size: 14,
                            color: Colors.orangeAccent,
                          )
                        : SizedBox.shrink(),
                    Text(
                      official || officialAide1Vallee
                          ? "OFFICIEL${textJ != "" ? " - " : ""}$textJ"
                          : textJ,
                      style:
                          TextStyle(fontSize: 12, fontWeight: FontWeight.bold),
                    )
                  ],
                )),
          ],
        ),
        Row(
          children: [
            Expanded(
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                  Text(demand.summary),
                  Container(
                      padding: const EdgeInsets.only(top: 5),
                      child: Text("${demand.date} - Durée:${demand.duration}h",
                          style: TextStyle(
                              color: Colors.grey,
                              fontSize: Theme.of(context)
                                  .textTheme
                                  .subtitle2
                                  .fontSize))),
                  Chip(
                    label: Text(demand.title),
                    avatar: FaIcon(getIconType(demand.title)),
                    backgroundColor: Color(0xFFCCCCCC),
                  ),
                  demand.hardAccess
                      ? Chip(
                          label: Text("Accès difficile"),
                          avatar: Icon(Icons.warning),
                          backgroundColor: Colors.yellow,
                        )
                      : SizedBox.shrink()
                ])),
            Column(
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Chip(
                    label: Text(
                      "Inscrits : $numberPeople / $numberRequired",
                      style: TextStyle(
                          color: Colors.white, fontWeight: FontWeight.bold),
                    ),
                    backgroundColor: getBackGroundColorParticipants()),
                Row(
                  children: [
                    Icon(
                      Icons.location_on,
                      size: 14,
                      color: Colors.green,
                    ),
                    Text(
                      demand.village.capitalizeFirst,
                    ),
                  ],
                )
              ],
            )
          ],
        )
      ]),
      onTap: () => Get.to(ViewDemand(demand.id)),
    );
  }

  IconData getIconType(String data) {
    switch (data) {
      case "Nettoyage":
        return Icons.cleaning_services;
      case "Aide à la personne":
        return Icons.people;
      case "Livraisons diverses":
        return Icons.shopping_cart;
      case "Livraisons premières nécéssités":
        return Icons.shopping_cart;
      case "Déblayage":
        return FontAwesomeIcons.road;
      case "Tronçonnage":
        return FontAwesomeIcons.tree;
      case "Réparations privées":
        return FontAwesomeIcons.tools;
      case "Réparations publiques":
        return FontAwesomeIcons.tools;
      case "Maçonnerie":
        return Icons.house;
      case "Proposition d'hébergement":
        return FontAwesomeIcons.bed;
      case "Covoiturage":
        return Icons.drive_eta;
      case "Autre":
        return Icons.stream;
    }
    return Icons.stream;
  }

  Color getBackGroundColorParticipants() {
    if (demand.isPastOrArchived()) {
      return Colors.grey;
    }
    var numberPeople = demand.people.length;
    var numberRequired = demand.numberRequired;
    return numberPeople < numberRequired ? Colors.red : Colors.green;
  }
}
