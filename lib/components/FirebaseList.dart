import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';

class FirebaseList<T> extends StatelessWidget {
  FirebaseList({this.query, this.listElementWidget, this.convert});

  final Query query;
  final T Function(DocumentSnapshot) convert;
  final Widget Function(T) listElementWidget;

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: query.snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text('Something went wrong');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Text("Loading");
        }

        return new ListView(
          children: snapshot.data.docs
              .map((DocumentSnapshot e) => convert(e))
              .map((T document) {
            return listElementWidget(document);
          }).toList(),
        );
      },
    );
  }
}
