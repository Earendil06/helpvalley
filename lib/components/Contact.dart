import 'package:flutter/material.dart';
import 'package:helpvalley/RemoteConfigEngine.dart';
import 'package:url_launcher/url_launcher.dart';

class Contact extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Row(children: [
          const Text("Email: "),
          TextButton(
              onPressed: () =>
                  launch(Uri(
                      scheme: 'mailto',
                      path: RemoteConfigEngine.instance.getString("email"),
                      queryParameters: {'subject': "Demande d'aide"})
                      .toString()),
              child: Text(RemoteConfigEngine.instance.getString("email")))
        ]),
        Row(children: [
          const Text("FAQ: "),
          TextButton(
              onPressed: () async {
                var url = RemoteConfigEngine.instance.getString("faq_link");
                if (await canLaunch(url)) {
                  await launch(url);
                } else {
                  throw 'Could not launch $url';
                }
              },
              child: const Text("Cliquez ici"))
        ]),
        Row(children: [
          const Text("Tel: "),
          TextButton(
              onPressed: () => launch("tel://" + RemoteConfigEngine.instance
                  .getString("contact_phone")),
              child: Text(RemoteConfigEngine.instance.getString("contact_phone")))
        ])
      ],
    );
  }
}
