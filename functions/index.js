const functions = require('firebase-functions');
const admin = require('firebase-admin');
const firestore = require('@google-cloud/firestore');

admin.initializeApp(functions.config().functions);

exports.updateTotalDemands = functions.firestore.document("/demands/{docID}")
    .onCreate((snapshot, context) => {
        admin.database()
            .ref()
            .update({
                "demandsNumber": admin.database.ServerValue.increment(1)
            });
    });

exports.updateTotalDemandsRemove = functions.firestore.document("/demands/{docID}")
    .onDelete((snapshot, context) => {
        admin.database()
            .ref()
            .update({
                "demandsNumber": admin.database.ServerValue.increment(-1)
            });
    });

function sendNotification(title, body, icon, tokens) {
    if (tokens !== undefined && tokens.length > 0) {
        return admin.messaging().sendToDevice(tokens,
            {
                notification: {
                    title: title,
                    body: body,
                    icon: icon
                }
            },
            {
                contentAvailable: true,
                priority: "high",
            }
        );
    }
    return Promise.reject("Bad token: " + tokens);
}

function computeMessage(previousValue, newValue) {
    if (!previousValue.canceled && newValue.canceled) {
        return "Demande annulée : " + newValue.summary;
    }
    if (previousValue.canceled && !newValue.canceled) {
        return "Demande réouverte : " + newValue.summary;
    }
    if (previousValue.information !== newValue.information && !newValue.canceled) {
        return "Informations mises à jour dans : " + newValue.summary;
    }
    if (previousValue.material !== newValue.material && !newValue.canceled) {
        return "Matériel mis a jour dans : " + newValue.summary;
    }
    if (previousValue.date !== newValue.date && !newValue.canceled) {
        return "Changement de date pour : " + newValue.summary;
    }
    return "";
}

exports.sendUpdateNotif = functions.firestore.document('/demands/{documentId}')
    .onUpdate(async (change, context) => {
        const newValue = change.after.data();
        const previousValue = change.before.data();

        let message = computeMessage(previousValue, newValue);

        if (message !== "") {
            const usersToNotify = await admin.firestore()
                .collection("users")
                .where("notificationsEnabled", "array-contains", "demand_changed")
                .get();

            const futures = [];

            for (const user of usersToNotify.docs.filter(f => newValue.people.includes(f.id))) {
                const tokens = user.data().tokens;
                if (tokens !== undefined && tokens.length > 0) {
                    futures.push(sendNotification(
                        "Changement d'informations dans une demande",
                        message,
                        'https://www.aide1vallee.fr/favicon.jpg',
                        tokens));
                }
            }

            Promise.all(futures)
                .then((values) => console.log(values))
                .catch(reason => console.log(reason));
        }
    });

function notificationIdFromName(valley) {
    switch (valley) {
        case "Vallée Roya":
            return "new_demand_roya";
        case "Vallée Vésubie":
            return "new_demand_vesubie";
        case "Vallée Var":
            return "new_demand_var";
        case "Vallée Tinée":
            return "new_demand_tinee";
    }
    return "";
}

exports.notifNewDemand = functions.firestore.document('/demands/{documentId}')
    .onCreate(async (snapshot, context) => {
        const valley = snapshot.data().place;
        const notificationId = notificationIdFromName(valley);
        const usersToNotify =
            await admin.firestore().collection("users")
                .where("notificationsEnabled", "array-contains", notificationId)
                .get();

        const futures = [];

        console.log("Notify " + usersToNotify.docs.length + " users");

        for (const user of usersToNotify.docs) {
            const tokens = user.data().tokens;
            if (tokens !== undefined && tokens.length > 0) {
                futures.push(sendNotification(
                    "Nouvelle demande",
                    "Nouvelle demande dans " + valley + ": " + snapshot.data().summary,
                    'https://www.aide1vallee.fr/favicon.jpg',
                    tokens));
            }
        }

        Promise.all(futures)
            .then((values) => console.log(values))
            .catch(reason => console.log(reason));
    })

exports.makeOfficialDemand = functions.firestore.document('/demands/{documentId}')
    .onCreate(async (snapshot, context) => {
        const authorID = snapshot.data().userID;
        const user = await admin.firestore().doc("users/" + authorID)
            .get();

        const role = user.data().role;
        console.log("user role is: " + role);

        return snapshot.ref.set({
            official: role === "official" || snapshot.data().official,
            officialAide1Vallee: role === "officialAide1Vallee" || snapshot.data().officialAide1Vallee
        }, {merge: true});
    });

const bucket = 'gs://help-valley-firestore-bak';
const client = new firestore.v1.FirestoreAdminClient();

exports.scheduledFirestoreExport = functions.pubsub
    .schedule('every 24 hours')
    .onRun((context) => {

        const projectId = process.env.GCP_PROJECT || process.env.GCLOUD_PROJECT;
        const databaseName =
            client.databasePath(projectId, '(default)');

        return client.exportDocuments({
            name: databaseName,
            outputUriPrefix: bucket,
            collectionIds: []
        })
            .then(responses => {
                const response = responses[0];
                console.log(`Operation Name: ${response['name']}`);
                return response;
            })
            .catch(err => {
                console.error(err);
                throw new Error('Export operation failed');
            });
    });

exports.tempChangeDeletedToArchivedDemand = functions.firestore.document('/demands/{documentId}')
    .onUpdate(async (change, context) => {
        const newValue = change.after.data();
        const previousValue = change.before.data();

        let justDeleted = newValue.canceled && !previousValue.canceled;

        if (justDeleted) {
            return change.after.ref.set({
                archived: true
            }, {merge: true});
        }
    });
